# nzcovidmodel

Pathogen propagation simulation for NZ, originally created for an outreach event at VUW in 2016. It is conceptually very basic and I was (and still am) a complete newcomer to ABM. Below follow a brief writeup of what was done back then.


## Concepts and ideas from the olde code from 2016

Two lists, people and locations, connected via a bi-partite graph. Here's the old summary slide:


![summary.png](summary.png)


### People 

Simple three-tiered demographic, total population $N$ divided down into children, adults, and retirees by an initial dice roll and probabilities $P(child)$, $P(adult)$, and $P(retired)$. The old code had me guesstimating the probabilities, we should be able to do much better. 
Each person had possible tags:
  - alive
  - dead
  - infected
  - infectious
  - symptomatic
  - immune
These tags would be updated in an update time-step, with probabilities to become infected in the presence of an infectious person, etc. The probabilities that govern the update are flat across the population (same for children and retirees). Again we can do better here.

Improvement ideas:
  - more categorisation related to age (e.g. age boundaries <1, <10, <20, <40, <60, <75, >75), where each age-group has distinct probabilities for pathogen transmission, symptom expression, mortality, etc, based on statistics 
  - more categories reflecting life/work-style regarding degree of interaction with others, e.g. back-office work, customer service, taxi/uber/etc.
  

### Locations

Locations were grabbed from openstreetmap, confined to the wellington region. The resulting ~50MB spreadsheet contained all polygonal data which was partly pre-catogorised into building types. I extracted a list of 
  - residential dwellings
  - schools
  - public transport (really only train stations)
  - and 'other'.

This can and should be improved. The openstreetmap data set likely has improved over the last four years. Other -potentially better- sources are available too, e.g. [mapbox](https://www.mapbox.com). Anyone familiar with it?

Also there is no notion of individual habits, e.g. uses own car vs public transport, or hermite vs socialite. Currently we have near-flat population-wide probabilities to visit any place. I think a good idea would be to set up a global matrix of probabilities, with locations in the columns and a row for each person. This would allow to encode individual habits to some extend, based on probabilities governing this. This matrix should be created only once; however it would allow for behavioural modifiers, e.g. social isolation kicks in, or even account for a day-night rhythm.


### Time-step

Here's what a single time-step in the old algo looks like. 

![algo_overview](algo_overview.png)

Currently time-steps have no unit, and, as stated further above, there is no modification of visitation probabilities happening as a function of time-elapsed or time-of-day.

It borders on spaghetti code, and if I could have used '''goto''' I would have. I think a couple of well-defined state-machines and relationships between them might clear this up and allow for flexible coding and evaluation. It does feel like reinventing the wheel of coupled Markov chains, and I suspect someone with a bit more modelling experience will be able to draw up a more suited algorithm.



---



